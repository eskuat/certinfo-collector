#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>

#include <limits.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/conf.h>
#include <openssl/x509.h>
#include <openssl/buffer.h>
#include <openssl/x509v3.h>
#include <openssl/opensslconf.h>

#include "queue.h"

int g_debug = 0;

int g_default_thn = 1;

int g_buffer_size = 8;

int g_connnect_timeout = 1;

pthread_mutex_t printf_mutex;

pthread_mutex_t *g_ssl_mutex_array = NULL;

int test_ssl();
int try_test_server();
void set_debug_level();
int scan_from_target_host();
int scan_from_stdin();
int threaded_scanner(); 

int setup_ssl_locking_func();

int myfprintf(FILE*, const char*, ...);

#define STATUS_OK                               0
#define STATUS_TEST_SERVER_CHECK_FAILED         1
#define STATUS_TARGET_HOST_PORT_NOT_SPECIFIED   2
#define STATUS_MALLOC_ERROR                     3
#define STATUS_PTHREAD_CREATE_ERROR             4
#define STATUS_PTHREAD_JOIN_ERROR               5
#define STATUS_BAD_INPUT                        6
#define STATUS_OPENSSL_ERROR                    7

#define debug_print(fmt, ...) \
    do { if (g_debug) \
            myprintf(stderr, "debug,%ld," fmt, time(NULL), __VA_ARGS__); \
    } while (0)

struct thread_info {
    pthread_t thread_id;
};

struct queue_element {
    char host[256];
    char sni_host[256];
    char port[8];
};

struct scan_range {
    struct in_addr network_addr;
    struct in_addr broadcast_addr;
    int port;
};

struct scan_list {
    struct queue_element *el; 
    struct scan_list *next;
};

/* certificate entries */
#define FIELD_ISSUER_NAME               1
#define FIELD_SUBJECT_NAME              2
#define FIELD_FINGERPRINT               3
#define FIELD_EXPIRY                    4
#define FIELD_SERIAL                    5
#define FIELD_KEYTYPE                   6

int main(int argc, char *argv[]) {
    int rv;

    pthread_mutex_init(&printf_mutex, NULL);
    rv = setup_ssl_locking_func();

    if(rv != STATUS_OK)
        return rv;

    if(getenv("DEBUG"))
        g_debug = 1;

    rv  = try_test_server();
    if (rv != STATUS_OK) {
        return STATUS_TEST_SERVER_CHECK_FAILED;
    }

    rv = scan_from_target_host();
    if (rv == STATUS_TARGET_HOST_PORT_NOT_SPECIFIED) 
        rv = scan_from_stdin();

    return rv;
}

int myprintf(FILE *stream, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    pthread_mutex_lock(&printf_mutex);
    int rv = vfprintf(stream, fmt, args);
    pthread_mutex_unlock(&printf_mutex);

    va_end(args);

    return rv;
}

void locking_function(int mode, int n, const char * file, int line) {
   if (mode & CRYPTO_LOCK) {
      pthread_mutex_lock(&g_ssl_mutex_array[n]);
   } else {
      pthread_mutex_unlock(&g_ssl_mutex_array[n]);
   }
}


int setup_ssl_locking_func(void) {
    int i;

    SSL_library_init();
    SSL_load_error_strings();
    OpenSSL_add_all_algorithms();

    g_ssl_mutex_array = OPENSSL_malloc(CRYPTO_num_locks() * sizeof (pthread_mutex_t));

    if (!g_ssl_mutex_array) {
        return STATUS_MALLOC_ERROR;
    }

    for (i = 0; i < CRYPTO_num_locks(); i++ ){
        pthread_mutex_init(&g_ssl_mutex_array[i], NULL);
    }

    CRYPTO_set_locking_callback(locking_function);

    return STATUS_OK;
}


int try_test_server() {
    char *test_server = getenv("TEST_SERVER");
    char *test_server_port = getenv("TEST_SERVER_PORT");
    debug_print("TEST_SERVER=%s,TEST_SERVER_PORT=%s\n",test_server, test_server_port);
    /* if one of var not set thats perfectly fine for us - we skip the check */
    if(!test_server || !test_server_port)
        return STATUS_OK;
    return test_ssl(NULL, test_server, test_server_port);
}


void* thread_start(void *arg) {
    void *task;

    while(NULL != (task = queue_dequeue(arg))) {
        struct queue_element *el = (struct queue_element *) task;
        debug_print("%ld thread got task %s:%s\n", pthread_self(), 
                el->host, el->port);
        if(strlen(el->sni_host))
            test_ssl(el->sni_host, el->host, el->port);
        else
            test_ssl(NULL, el->host, el->port);
        free(task);
    }

    return NULL;
}


int scan_from_target_host() { 
    char *target_host = getenv("TARGET_HOST");
    char *target_port = getenv("TARGET_PORT");
    struct in_addr addr, mask, network_addr, broadcast_addr;

    if(!target_host || !target_port) {
        return STATUS_TARGET_HOST_PORT_NOT_SPECIFIED;
    }

    char *pos = strchr(target_host, '/');
    if(!pos) {
        return STATUS_TARGET_HOST_PORT_NOT_SPECIFIED;
    }
    int mask_width = atoi(pos+1);
    char *network = strndup(target_host, pos - target_host);

    debug_print("network:%s,maskwidth:%d,target_port:%s\n",
            network, mask_width, target_port);

    if(!inet_aton(network, &addr)) {
        free(network);
        return STATUS_TARGET_HOST_PORT_NOT_SPECIFIED;
    }

    mask.s_addr = htonl(~((1 << (32 - mask_width)) - 1));
    network_addr.s_addr = addr.s_addr & mask.s_addr; 
    broadcast_addr.s_addr = (addr.s_addr & mask.s_addr) | ~(mask.s_addr); 

    /*actual scanning will use [minaddr+1, maxaddr-1] range of ip addresses*/
    debug_print("mask:%s\n", inet_ntoa(mask));
    debug_print("minaddr:%s\n",inet_ntoa(network_addr));
    debug_print("maxaddr:%s\n",inet_ntoa(broadcast_addr)); 

    struct scan_range scan_task;
    scan_task.network_addr = network_addr;
    scan_task.broadcast_addr = broadcast_addr;
    scan_task.port = atoi(target_port);

    int rv = threaded_scanner(NULL, &scan_task);

    free(network);
    return rv;
}

struct scan_list* add_to_scan_list(struct scan_list *tail, char *sni_host, char *host, char *port) {

    if (!tail) {
        /* HEAD */
        tail = malloc(sizeof(struct scan_list));
    } else {
        tail->next = malloc(sizeof(struct scan_list));
        tail = tail->next;
    }
    tail->next = NULL;
    tail->el = malloc(sizeof(struct queue_element));
    if (sni_host)
        strcpy(tail->el->sni_host, sni_host);
    else
        memset(tail->el->sni_host, '\0', 256);

    strcpy(tail->el->host, host);
    strcpy(tail->el->port, port);
    return tail;
}

void free_scan_list(struct scan_list *head) {
    struct scan_list *tail = head;
    struct scan_list *self;
    while(tail) {
        free(tail->el);
        self = tail;
        tail = tail->next;
        free(self);
    }
}

int scan_from_stdin() {
    char buf[512];
    char sni_host[256];
    char host[256];
    char port[8];
    struct scan_list *head = NULL;
    struct scan_list *tail = NULL;

    while(fgets(buf, 512, stdin)) {
        /* skip short lines */
        if(strlen(buf) < 3)
            continue;
        if(sscanf(buf, "%[^:]:%[^:]:%s", sni_host, host, port) == 3) {
            debug_print("scan from stdin: sni_host:%s,host:%s,port:%s\n",sni_host, host, port);
            tail = add_to_scan_list(tail, sni_host, host, port);
        } else if(sscanf(buf, "%[^:]:%s", host, port) == 2) {
            debug_print("scan from stdin: host:%s,port:%s\n", host, port);
            tail = add_to_scan_list(tail, NULL, host, port);
        } else {
            debug_print("input unrecognized:%s\n", buf);
            return STATUS_BAD_INPUT;
        }

        if (!head) {
            head = tail;
        }
    }

    int rv = threaded_scanner(head, NULL);

    free_scan_list(head);

    return rv;
}


int threaded_scanner(struct scan_list *scan_list, struct scan_range *scan_range) {
    struct in_addr network_addr, broadcast_addr;
    int port;
    char *thn = getenv("THN");
    int tnum, s, num_threads = 0;
    void *buffer[g_buffer_size];
    struct thread_info *tinfo;
    struct in_addr scan_addr;
    queue_t queue = QUEUE_INITIALIZER(buffer);

    if(thn)
        num_threads = atoi(thn);
    if(!num_threads)
        num_threads = g_default_thn;

    tinfo = calloc(num_threads, sizeof(struct thread_info));
    if (!tinfo) {
        debug_print("calloc() failed %d\n", errno);
        return STATUS_MALLOC_ERROR;
    }

    if (scan_range) {
        network_addr = scan_range->network_addr;
        broadcast_addr = scan_range->broadcast_addr;
        port = scan_range->port;
    }

    for (tnum = 0; tnum < num_threads; tnum++) {
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN * 4);

        s = pthread_create(&tinfo[tnum].thread_id, &attr, &thread_start, &queue);

        if (s != 0) {
            debug_print("pthread_create() failed %d\n", s);
            return STATUS_PTHREAD_CREATE_ERROR;
        }
        debug_print("%ld thread created\n", tinfo[tnum].thread_id);
    }

    if (scan_range) {
        //scan_addr.s_addr = htonl(ntohl(network_addr.s_addr) + 1);
        scan_addr.s_addr = network_addr.s_addr;
        for(;scan_addr.s_addr <= broadcast_addr.s_addr; scan_addr.s_addr = htonl(ntohl(scan_addr.s_addr)+1)) {
            /*
            struct sockaddr_in *a = malloc(sizeof(struct sockaddr_in));
            a->sin_addr = scan_addr;
            a->sin_port = htons(port);*/
            struct queue_element *a = malloc(sizeof(struct queue_element));
            memset((void *)a, '\0', sizeof(struct queue_element));
            inet_ntop(AF_INET, &scan_addr, a->host, 256);
            memset((void *) a->sni_host, '\0', 256);
            snprintf(a->port, 8, "%u", port);
            queue_enqueue(&queue, a);
        }
    } else if (scan_list) {
        struct scan_list *tail = scan_list;
        while(tail) {
            queue_enqueue(&queue, tail->el);
            tail = tail->next;
        }
    } else {
        /* neither scan_list nor scan_range specified */
        return STATUS_OK;
    }
    /* num_threads times send NULL to queue telling threads to exit */
    for (tnum = 0; tnum < num_threads; tnum++)
        queue_enqueue(&queue, NULL); 

    for (tnum = 0; tnum < num_threads; tnum++) {
        s = pthread_join(tinfo[tnum].thread_id, NULL);
        if (s != 0) {
            debug_print("%ld thread failed to stop\n", tinfo[tnum].thread_id);
            return STATUS_PTHREAD_JOIN_ERROR;
        }
        debug_print("%ld thread stopped\n", tinfo[tnum].thread_id);
    }

    free(tinfo);
    return STATUS_OK;
}

void handle_failure(unsigned long err, char *host, char *port, char *label) {
    const char* const str = ERR_reason_error_string(err);
    if(str)
        debug_print("%s:%s %s\n", host, port, str);
    else
        debug_print("%s:%s %s failed: %lu (0x%lx)\n", host, port, label, err, err);
}

void print_cn_name(const char* label, int field_id, X509_NAME* const name) {
    int idx = -1, success = 0;
    unsigned char *utf8 = NULL;
    
    do {
        if(!name) break; /* failed */
        
        idx = X509_NAME_get_index_by_NID(name, NID_commonName, -1);
        if(!(idx > -1))  break; /* failed */
        
        X509_NAME_ENTRY* entry = X509_NAME_get_entry(name, idx);
        if(!entry) break; /* failed */
        
        ASN1_STRING* data = X509_NAME_ENTRY_get_data(entry);
        if(!data) break; /* failed */
        
        int length = ASN1_STRING_to_UTF8(&utf8, data);
        if(!utf8 || !(length > 0))  break; /* failed */
        
        myprintf(stdout, "%s/%d/%s\n", label, field_id, utf8);
        success = 1;
        
    } while (0);
    
    if(utf8)
        OPENSSL_free(utf8);
    
    if(!success)
        myprintf(stdout, "%s/%d/<not available>\n", label, field_id);
}

void print_expiry(const char *label, int field_id, X509 *x) {
    ASN1_TIME *notafter = X509_get_notAfter(x);
    ASN1_TIME *notbefore = X509_get_notBefore(x);
    BIO *bio;
    int write = 0;
    char buf[128];
    bio = BIO_new(BIO_s_mem());
    if (bio) {
        ASN1_TIME_print(bio, notbefore);
        BIO_write(bio, "/", 1);
        ASN1_TIME_print(bio, notafter);
        write = BIO_read(bio, buf, 128-1);
        BIO_free(bio);
    }
    buf[write]='\0';
    myprintf(stdout, "%s/%d/%s\n", label, field_id, buf);
}

void print_serial(const char *label, int field_id, X509 *x) {
    long l;
    const char *neg;
    int i;
    ASN1_INTEGER *bs = X509_get_serialNumber(x);
    if (bs->length <= (int)sizeof(long)) {
        l = ASN1_INTEGER_get(bs);
        if (bs->type == V_ASN1_NEG_INTEGER) {
            l = -l;
            neg = "-";
        } else
            neg = "";
        myprintf(stdout, "%s/%d/%s%lu (%s0x%lx)\n", label, field_id, neg, l, neg, l);
    } else {
        neg = (bs->type == V_ASN1_NEG_INTEGER) ? " (Negative)" : "";
        myprintf(stdout, "%s/%d/%s", label, field_id, neg);

        for (i = 0; i < bs->length; i++) {
            myprintf(stdout, "%02x%c", bs->data[i],
                        ((i + 1 == bs->length) ? '\n' : ':'));
        }
    }
}

void print_keytype(const char *label, int field_id, X509 *x) {
    X509_CINF *ci;
    BIO *bio;
    int write = 0;
    char buf[32];
    ci = x->cert_info;
    bio = BIO_new(BIO_s_mem());
    if (bio) {
        if (i2a_ASN1_OBJECT(bio, ci->key->algor->algorithm))
            write = BIO_read(bio, buf, 32-1);
        BIO_free(bio);
    }
    buf[write]='\0';
    myprintf(stdout, "%s/%d/%s\n", label, field_id, buf);
}

void print_fingerprint(const char *label, int field_id, X509 *x) {
    int j;
    unsigned int n;
    unsigned char buf[EVP_MAX_MD_SIZE];
    const EVP_MD *fdig = EVP_sha1();

    X509_digest(x, fdig, buf, &n);

    myprintf(stdout, "%s/%d/",label, field_id);
    for (j = 0; j < (int)n; j++) {
        myprintf(stdout, "%02X%c", buf[j], (j + 1 == (int)n)
                ? '\n' : ':');
    }
}

void print_cert_info(char *host, char *ip, char *port, X509 *x) {
    char label[256];
    char buf[4096];
    int n = 0;
    if(host)
        n = snprintf(label, 256, "%s:%s:%s", host, ip, port);
    else
        n = snprintf(label, 256, "%s:%s", ip, port);
    label[n] = '\0';

    X509_NAME_oneline(X509_get_issuer_name(x), buf, sizeof buf);
    myprintf(stdout, "%s/%d/issuer=%s\n", label, FIELD_ISSUER_NAME, buf);
    X509_NAME_oneline(X509_get_subject_name(x), buf, sizeof buf);
    myprintf(stdout, "%s/%d/subject=%s\n", label, FIELD_SUBJECT_NAME, buf);

    /*
    X509_NAME* iname = x ? X509_get_issuer_name(x) : NULL;
    X509_NAME* sname = x ? X509_get_subject_name(x) : NULL;

    print_cn_name(label, FIELD_ISSUER_NAME, iname);
    print_cn_name(label, FIELD_SUBJECT_NAME, sname);
    */

    print_fingerprint(label, FIELD_FINGERPRINT, x);

    print_expiry(label, FIELD_EXPIRY, x);
    print_serial(label, FIELD_SERIAL, x);
    print_keytype(label, FIELD_KEYTYPE, x);
}

int test_ssl(char *sni_host, char *ip, char *port) {
    long res = 1;

    SSL_CTX* ctx = NULL;
    BIO *web = NULL;
    SSL *ssl = NULL;
    unsigned long ssl_err = 0;
    fd_set confds;
    struct timeval tv;
    int fd;
    int rv = STATUS_OK;

    const SSL_METHOD* method = SSLv23_method();
    ssl_err = ERR_get_error();
    if(!(NULL != method)) {
        handle_failure(ssl_err, ip, port, "SSLv23_method");
        return STATUS_OPENSSL_ERROR;
    }

    ctx = SSL_CTX_new(method);
    ssl_err = ERR_get_error();
    if(!(ctx != NULL)) {
        handle_failure(ssl_err, ip, port, "SSL_CTX_new");
        return STATUS_OPENSSL_ERROR;
    }

    web = BIO_new_ssl_connect(ctx);
    ssl_err = ERR_get_error();
    if(!(web != NULL)) {
        handle_failure(ssl_err, ip, port, "BIO_new_ssl_connect");
        return STATUS_OPENSSL_ERROR;
    }

    res = BIO_set_conn_hostname(web, ip);
    ssl_err = ERR_get_error();
    if(!(1 == res)) {
        handle_failure(ssl_err, ip, port, "BIO_set_conn_hostname");
        return STATUS_OPENSSL_ERROR;
    }

    res = BIO_set_conn_port(web, port);
    ssl_err = ERR_get_error();
    if(!(1 == res)) {
        handle_failure(ssl_err, ip, port, "BIO_set_conn_port");
        return STATUS_OPENSSL_ERROR;
    }

    BIO_get_ssl(web, &ssl);
    ssl_err = ERR_get_error();
    if(!(ssl != NULL)) {
        handle_failure(ssl_err, ip, port, "BIO_set_ssl");
        return STATUS_OPENSSL_ERROR;
    }

    if(sni_host) {
        debug_print("%ld thread setting sni %s for %s:%s\n", pthread_self(), 
                sni_host, ip, port);
        res = SSL_set_tlsext_host_name(ssl, sni_host);
        ssl_err = ERR_get_error();
        if(!(1 == res)) {
            handle_failure(ssl_err, ip, port, "BIO_set_ssl");
            return STATUS_OPENSSL_ERROR;
        }
    }

    BIO_set_nbio(web, 1);

    res = BIO_do_connect(web);
    ssl_err = ERR_get_error();
    if(!BIO_should_retry(web) && !(1 == res)) {
        handle_failure(ssl_err, ip, port, "BIO_do_connect");
        rv = STATUS_OPENSSL_ERROR;
        goto outofhere;
    }

    if(BIO_get_fd(web, &fd) <= 0) {
        handle_failure(BIO_R_UNINITIALIZED, ip, port, "BIO_get_fd");
        rv = STATUS_OPENSSL_ERROR;
        goto outofhere;
    }

    FD_ZERO(&confds);
    FD_SET(fd, &confds);
    tv.tv_usec = 0;
    tv.tv_sec = g_connnect_timeout;
    if(!select(fd + 1, NULL, (void *)&confds, NULL, &tv)) {
        handle_failure(BIO_R_CONNECT_ERROR, ip, port, "Connection timed out");
        rv = STATUS_OPENSSL_ERROR;
        goto outofhere;
    }

    int handshake_start = time(NULL);
    while(1) {
        res = BIO_do_handshake(web);
        ssl_err = ERR_get_error();
        if (BIO_should_retry(web)) {
            if((time(NULL) - handshake_start) > g_connnect_timeout + 1) {
                rv = STATUS_OPENSSL_ERROR;
                goto outofhere;
            } else {
                continue;
            }
        }

        if(!(1 == res)) {
            handle_failure(ssl_err, ip, port, "BIO_do_handshake");
            rv = STATUS_OPENSSL_ERROR;
            goto outofhere;
        }
        break;
    }

    debug_print("%ld thread established connection with %s:%s\n", 
            pthread_self(), ip, port);

    X509* cert = SSL_get_peer_certificate(ssl);
    if(cert) { 
        print_cert_info(sni_host, ip, port, cert); 
        X509_free(cert); 
    } 
    if(NULL == cert) {
        handle_failure(X509_V_ERR_APPLICATION_VERIFICATION, ip, port, "SSL_get_peer_certificate");
    }

outofhere:
    if(web != NULL)
        BIO_free_all(web);

    if(NULL != ctx)
        SSL_CTX_free(ctx);

    return rv;
}
