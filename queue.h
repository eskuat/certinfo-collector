#ifndef _QUEUE_H
#define _QUEUE_H

#include <pthread.h>

#define QUEUE_INITIALIZER(buffer) { \
    buffer, \
    sizeof(buffer) / sizeof(buffer[0]), \
    0, 0, 0, \
    PTHREAD_MUTEX_INITIALIZER, \
    PTHREAD_COND_INITIALIZER, \
    PTHREAD_COND_INITIALIZER }

typedef struct queue {
    void **buffer;
    const int capacity;
    int size;
    int in;
    int out;
    pthread_mutex_t mutex;
    pthread_cond_t cond_full;
    pthread_cond_t cond_empty;
} queue_t;

void queue_enqueue(queue_t *queue, void *value) {
    pthread_mutex_lock(&(queue->mutex));
    while (queue->size == queue->capacity)
        pthread_cond_wait(&(queue->cond_full), &(queue->mutex));
    queue->buffer[queue->in] = value;
    ++ queue->size;
    ++ queue->in;
    queue->in %= queue->capacity;
    pthread_mutex_unlock(&(queue->mutex));
    pthread_cond_broadcast(&(queue->cond_empty));
}

void *queue_dequeue(queue_t *queue) {
    pthread_mutex_lock(&(queue->mutex));
    while (queue->size == 0)
            pthread_cond_wait(&(queue->cond_empty), &(queue->mutex));
    void *value = queue->buffer[queue->out];
    -- queue->size;
    ++ queue->out;
    queue->out %= queue->capacity;
    pthread_mutex_unlock(&(queue->mutex));
    pthread_cond_broadcast(&(queue->cond_full));
    return value;
}

#endif


